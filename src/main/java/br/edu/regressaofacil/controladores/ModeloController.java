package br.edu.regressaofacil.controladores;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import br.edu.regressaofacil.dao.ModeloDao;
import br.edu.regressaofacil.dao.UsuarioDao;
import br.edu.regressaofacil.modelos.Modelo;
import br.edu.regressaofacil.modelos.Usuario;

@Transactional
@Controller
public class ModeloController {

	@Autowired
	ModeloDao modeloDao;
	@Autowired
	UsuarioDao usuarioDao;

	@RequestMapping("modelo")
	public String telaModelo(HttpSession session, Model model) {
		Usuario usuario = usuarioDao.getObjeto((Integer) session.getAttribute("idUsuario"));
		model.addAttribute("usuario", usuario);
		return "modelos";
	}

	@RequestMapping("novo-modelo")
	public String telaCadastroModelo() {
		return "novo-modelo";
	}

	@RequestMapping("altera-modelo")
	public String telaAlteraModelo(@RequestParam("idModelo") int idModelo, Model model) {
		Modelo modelo = modeloDao.getObjeto(idModelo);
		model.addAttribute("modelo", modelo);
		return "altera-modelo";
	}

	private boolean isErros(BindingResult result) {
		return result.hasFieldErrors("nome") && result.hasFieldErrors("formula")
				&& result.hasFieldErrors("funcaoLigacao") && result.hasFieldErrors("familiaDistribuicao");
	}

	@RequestMapping(path = "cadastraModelo", method = RequestMethod.POST)
	public String cadastraModelo(@Valid Modelo modelo, BindingResult result, HttpSession session, Model model,
			HttpServletRequest request) {
		MultipartHttpServletRequest requestMultipart = (MultipartHttpServletRequest) request;
		if (!isErros(result) && requestMultipart.getMultipartContentType("arquivo").contains("csv")) {
			Usuario usuario = usuarioDao.getObjeto((Integer) session.getAttribute("idUsuario"));
			try {
				modelo.setArquivo(requestMultipart.getFile("arquivo").getBytes());
			} catch (IOException e) {
				model.addAttribute("cadastroModelo", false);
				return "novo-modelo";
			}
			usuario.addModelo(modelo);
			usuarioDao.atualizaObjeto(usuario);
			model.addAttribute("cadastroModelo", true);
			return "novo-modelo";
		} else {
			model.addAttribute("cadastroModelo", false);
			return "novo-modelo";
		}
	}

	@RequestMapping(path = "alterarModelo", method = RequestMethod.POST)
	public String alteraModelo(@Valid Modelo modelo, BindingResult result, HttpSession session, Model model,
			HttpServletRequest request) {
		MultipartHttpServletRequest requestMultipart = (MultipartHttpServletRequest) request;
		if (!isErros(result)) {
			if (requestMultipart.getFile("arquivo").getSize() > 0) {
				try {
					modelo.setArquivo(requestMultipart.getFile("arquivo").getBytes());
				} catch (IOException e) {
					model.addAttribute("alteraModelo", false);
					return "altera-modelo";
				}
			}
			model.addAttribute("alteraModelo", true);
			modeloDao.atualizaObjeto(modelo);
		} else {
			model.addAttribute("alteraModelo", false);
		}
		return "altera-modelo";
	}

	@RequestMapping("remove-modelo")
	@ResponseBody
	public void removeModelo(@RequestParam("idModelo") int idModelo) {
		modeloDao.removeObjeto(idModelo);
	}

	private ResponseEntity<InputStreamResource> geraPdfModelo(StringBuilder conteudoModelo, Image... imagens) {
		if (imagens.length != 4)
			throw new IllegalArgumentException(
					"O número exato de imagens esperadas" + " para a criação do PDF que representa o modelo é '4'.");
		Document documento = new Document();
		ByteArrayOutputStream saida = new ByteArrayOutputStream();
		PdfWriter.getInstance(documento, saida);
		System.out.println(conteudoModelo.toString());

		documento.open();
		documento.add(new Paragraph(conteudoModelo.toString()));
		documento.add(imagens[0]);
		documento.add(imagens[1]);
		documento.add(imagens[2]);
		documento.add(imagens[3]);
		documento.close();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=file.pdf");
		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(new ByteArrayInputStream(saida.toByteArray())));
	}

	@RequestMapping(path = "executa-modelo", method = RequestMethod.GET)
	@ResponseBody
	@SuppressWarnings("all")
	public ResponseEntity executaModelo(@RequestParam("idModelo") int idModelo) {
		try {
			Modelo modelo = modeloDao.getObjeto(idModelo);
			String caminhoTmp = Files.createTempDirectory(System.currentTimeMillis() + "").toString();
			System.out.println(caminhoTmp);
			BufferedOutputStream eDados = new BufferedOutputStream(new FileOutputStream(caminhoTmp + "/dados.csv"));
			eDados.write(modelo.getArquivo());
			eDados.close();

			BufferedWriter eScript = new BufferedWriter(new FileWriter(caminhoTmp + "/script.r"));
			eScript.write("options(warn=-1)\n");
			eScript.write(
					String.format("dataBase <- read.csv(file=\"%s/dados.csv\", header=TRUE, sep=\",\")", caminhoTmp));
			eScript.write("\n");
			eScript.write(String.format("result <- glm(I(%s), family=%s(\"%s\"), data=dataBase)", modelo.getFormula(),
					modelo.getFamiliaDistribuicao(), modelo.getFuncaoLigacao()));
			eScript.write("\n");
			eScript.write(String.format("jpeg(\"%s/image1.jpg\")", caminhoTmp));
			eScript.write("\n");
			eScript.write("plot(result, which=1)");
			eScript.write("\n");
			eScript.write(String.format("jpeg(\"%s/image2.jpg\")", caminhoTmp));
			eScript.write("\n");
			eScript.write("plot(result, which=2)");
			eScript.write("\n");
			eScript.write(String.format("jpeg(\"%s/image3.jpg\")", caminhoTmp));
			eScript.write("\n");
			eScript.write("plot(result, which=3)");
			eScript.write("\n");
			eScript.write(String.format("jpeg(\"%s/image4.jpg\")", caminhoTmp));
			eScript.write("\n");
			eScript.write("plot(result, which=4)");
			eScript.write("\n");
			eScript.write(String.format("sink(file=\"%s/summary.txt\")", caminhoTmp));
			eScript.write("\n");
			eScript.write("summary(result)");
			eScript.write("\n");
			eScript.write("sink()");
			eScript.write("\n");
			eScript.flush();
			eScript.close();

			Process processo = Runtime.getRuntime().exec(new String[] { "Rscript", caminhoTmp + "/script.r" });
			int resultadoDaOperacao = 0;
			try {
				System.out.println(resultadoDaOperacao = processo.waitFor());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (resultadoDaOperacao == 0) {
				Image imagem1 = Image.getInstance(caminhoTmp + "/image1.jpg");
				Image imagem2 = Image.getInstance(caminhoTmp + "/image2.jpg");
				Image imagem3 = Image.getInstance(caminhoTmp + "/image3.jpg");
				Image imagem4 = Image.getInstance(caminhoTmp + "/image4.jpg");

				BufferedReader leitor = new BufferedReader(new FileReader(caminhoTmp + "/summary.txt"));
				StringBuilder string = new StringBuilder();
				while (leitor.ready())
					string.append(leitor.readLine() + "\n");
				leitor.close();
				return geraPdfModelo(string, imagem1, imagem2, imagem3, imagem4);
			} else {
				Thread.sleep(20000);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Content-Disposition", "inline; filename=file.pdf");
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(headers)
						.contentType(MediaType.APPLICATION_PDF)
						.body(new InputStreamResource(new ByteArrayInputStream(new byte[0])));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
