package br.edu.regressaofacil.controladores;

import java.util.Calendar;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.regressaofacil.dao.UsuarioDao;
import br.edu.regressaofacil.modelos.Token;
import br.edu.regressaofacil.modelos.Usuario;
import br.edu.regressaofacil.util.Util;

@Transactional
@Controller
public class UsuarioController {

	@Autowired
	private UsuarioDao usuarioDao;

	@RequestMapping("entrar")
	public String telaEntrar() {
		return "entrar";
	}

	@RequestMapping("esqueci-minha-senha")
	public String telaEsqueciMinhaSenha() {
		return "esqueci-minha-senha";
	}

	@RequestMapping("nova-senha")
	public String telaNovaSenha() {
		return "nova-senha";
	}

	@RequestMapping("logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:entrar";
	}

	@RequestMapping(path = "autenticaUsuario", method = RequestMethod.POST)
	public String autenticaUsuario(@RequestParam("email") String email, @RequestParam("senha") String senha,
			HttpSession session, Model model) {
		Usuario usuario = usuarioDao.autenticaUsuario(email, senha);
		if (usuario != null) {
			session.setAttribute("idUsuario", usuario.getId());
			session.setAttribute("nomeUsuario", usuario.getNome());
			return "redirect:modelo";
		} else {
			model.addAttribute("acessoNegado", true);
			return "entrar";
		}
	}

	@RequestMapping(path = "cadastraUsuario", method = RequestMethod.POST)
	public String cadastraUsuario(@Valid Usuario usuario, BindingResult result, Model model) {
		if (!result.hasErrors()) {
			if (usuarioDao.isEmailCadastrado(usuario)) {
				model.addAttribute("emailExiste", true);
			} else {
				usuarioDao.insereObjeto(usuario);
				model.addAttribute("cadastro", true);
			}
		}
		return "entrar";
	}

	@RequestMapping(path = "solicitaTrocaSenha", method = RequestMethod.POST)
	public String solicitaTrocaSenha(@RequestParam("email") String email, Model model) {
		Usuario usuario = usuarioDao.getUsuarioPorEmail(email);
		if (usuario != null) {
			Token token = new Token();
			token.setEstado(true);
			token.setCodigo(Util.aplicaHashMD5(Calendar.getInstance().getTimeInMillis() + "" + usuario.getId()));
			usuario.addToken(token);
			usuarioDao.atualizaObjeto(usuario);
			Util.enviaSolicitacaoRecuperacaoSenha(email, token.getCodigo());
			model.addAttribute("solicitacao", true);
		} else {
			model.addAttribute("solicitacao", false);
		}
		return "esqueci-minha-senha";
	}

	@RequestMapping(path = "novaSenha", method = RequestMethod.POST)
	public String novaSenha(@RequestParam("codigo") String codigo, @RequestParam("email") String email,
			@RequestParam("senha") String senha, Model model) {
		try {
			usuarioDao.trocaSenha(email, senha, codigo);
			model.addAttribute("trocaSenha", true);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("trocaSenha", false);
		}
		return "nova-senha";
	}

}
