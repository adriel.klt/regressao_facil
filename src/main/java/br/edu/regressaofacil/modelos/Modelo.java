package br.edu.regressaofacil.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "Modelo")
public class Modelo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@NotEmpty(message = "*Campo obrigatório")
	@Column(name = "nome")
	private String nome;
	@NotEmpty(message = "*Campo obrigatório")
	@Column(name = "formula")
	private String formula;
	@NotEmpty
	@Column(name = "funcaoLigacao")
	private String funcaoLigacao;
	@NotEmpty
	@Column(name = "familiaDistribuicao")
	private String familiaDistribuicao;
	@Lob
	@Column(name = "arquivo")
	private byte[] arquivo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getFuncaoLigacao() {
		return funcaoLigacao;
	}

	public void setFuncaoLigacao(String funcaoLigacao) {
		this.funcaoLigacao = funcaoLigacao;
	}

	public String getFamiliaDistribuicao() {
		return familiaDistribuicao;
	}

	public void setFamiliaDistribuicao(String familiaDistribuicao) {
		this.familiaDistribuicao = familiaDistribuicao;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

}
