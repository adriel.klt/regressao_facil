package br.edu.regressaofacil.modelos;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@NotEmpty(message="*Campo obrigatório")
	@Column(name = "nome")
	private String nome;
	@Pattern(regexp = "[[a-z0-9]+.*[a-z0-9]+]+@[[a-z0-9]+.*[a-z0-9]+]+", message="*Formato inválido")
	@Column(name = "email")
	private String email;
	@NotEmpty(message="*Campo obrigatório")
	@Column(name = "senha")
	@Size(min = 8, message="*O mínimo é 8 caractester")
	private String senha;
	@OneToMany(cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private List<Modelo> modelos = new ArrayList<Modelo>();
	@OneToMany(cascade = CascadeType.MERGE )
	@JoinColumn(name = "id_usuario")
	private List<Token> tokens = new ArrayList<Token>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		if (senha != null && !senha.isEmpty() && senha.length() >= 8) {
			try {
				MessageDigest digest = MessageDigest.getInstance("MD5");
				digest.update(senha.getBytes());
				this.senha = new BigInteger(1, digest.digest()).toString(16);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Modelo> getModelos() {
		return modelos;
	}

	public void addModelo(Modelo modelo) {
		this.modelos.add(modelo);
	}

	public List<Token> getTokens() {
		return tokens;
	}

	public void addToken(Token token) {
		this.tokens.add(token);
	}
	
	public int getQuantidadeModelos() {
		return modelos.size();
	}

}
