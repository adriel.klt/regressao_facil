package br.edu.regressaofacil.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.edu.regressaofacil.modelos.Modelo;

@Repository
public class ModeloDao implements Dao<Modelo> {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void insereObjeto(Modelo objeto) {
		entityManager.persist(objeto);
	}

	@Override
	public void atualizaObjeto(Modelo objeto) {
		if (objeto.getArquivo() == null)
			objeto.setArquivo(entityManager.find(Modelo.class, objeto.getId()).getArquivo());
		entityManager.merge(objeto);
	}

	@Override
	public void removeObjeto(int id) {
		entityManager.remove(entityManager.find(Modelo.class, id));
		entityManager.flush();
	}

	@Override
	public Modelo getObjeto(int id) {
		Modelo modelo = entityManager.find(Modelo.class, id);
		return modelo == null ? null : modelo;
	}

	@Override
	public List<Modelo> getTodosOsObjetos() {
		TypedQuery<Modelo> query = entityManager.createQuery("select m from Modelo m", Modelo.class);
		List<Modelo> lista = query.getResultList();
		return lista;
	}

	@Override
	public void fechaConexao() {
	}
}
