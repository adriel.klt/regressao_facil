package br.edu.regressaofacil.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.edu.regressaofacil.modelos.Token;
import br.edu.regressaofacil.modelos.Usuario;
import br.edu.regressaofacil.util.Util;

@Repository
public class UsuarioDao implements Dao<Usuario> {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void insereObjeto(Usuario objeto) {
		entityManager.persist(objeto);
	}

	@Override
	public void atualizaObjeto(Usuario objeto) {
		entityManager.merge(objeto);
	}

	@Override
	public void removeObjeto(int id) {
		entityManager.remove(entityManager.find(Usuario.class, id));
	}

	@Override
	public Usuario getObjeto(int id) {
		Usuario usuario = entityManager.find(Usuario.class, id);
		entityManager.refresh(usuario);
		return usuario == null ? null : usuario;
	}

	@Override
	public List<Usuario> getTodosOsObjetos() {
		TypedQuery<Usuario> query = entityManager.createQuery("select u from Usuario u", Usuario.class);
		List<Usuario> usuarios = query.getResultList();
		return usuarios;
	}

	@Override
	public void fechaConexao() {
	}

	public boolean isEmailCadastrado(Usuario usuario) {
		TypedQuery<Usuario> query = entityManager.createQuery("select u from Usuario u where u.email=:email",
				Usuario.class);
		query.setParameter("email", usuario.getEmail());
		boolean estado = query.getResultList().size() != 0;
		return estado;
	}

	public Usuario autenticaUsuario(String email, String senha) {
		TypedQuery<Usuario> query = entityManager
				.createQuery("select u from Usuario u where u.email=:email and u.senha=:senha", Usuario.class);
		query.setParameter("email", email);
		query.setParameter("senha", Util.aplicaHashMD5(senha));
		List<Usuario> lista = query.getResultList();
		entityManager.close();
		return lista.size() != 0 ? lista.get(0) : null;
	}

	public void trocaSenha(String email, String senha, String codigo) {
		TypedQuery<Token> query = entityManager
				.createQuery("select t from Token t where t.codigo=:codigo and t.estado=:estado", Token.class);
		query.setParameter("codigo", codigo);
		query.setParameter("estado", true);
		List<Token> lista = query.getResultList();
		if (!lista.isEmpty()) {
			Token token = lista.get(0);
			token.setEstado(false);
			entityManager.getTransaction().begin();
			entityManager.merge(token);
			entityManager.getTransaction().commit();
			entityManager.getTransaction().begin();
			Usuario usuario = getUsuarioPorEmail(email);
			usuario.setSenha(senha);
			entityManager.merge(usuario);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public Usuario getUsuarioPorEmail(String email) {
		TypedQuery<Usuario> query = entityManager.createQuery("select u from Usuario u where u.email=:email",
				Usuario.class);
		query.setParameter("email", email);
		List<Usuario> lista = query.getResultList();
		return lista.size() != 0 ? lista.get(0) : null;
	}

}
