package br.edu.regressaofacil.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.edu.regressaofacil.modelos.Token;

@Repository
public class TokenDao implements Dao<Token> {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void insereObjeto(Token objeto) {
		entityManager.persist(objeto);
	}

	@Override
	public void atualizaObjeto(Token objeto) {
		entityManager.merge(objeto);
	}

	@Override
	public void removeObjeto(int id) {
		entityManager.remove(getObjeto(id));
	}

	@Override
	public Token getObjeto(int id) {
		Token token = entityManager.find(Token.class, id);
		return token == null ? null : token;
	}

	@Override
	public List<Token> getTodosOsObjetos() {
		TypedQuery<Token> query = entityManager.createQuery("select t from Token t", Token.class);
		return query.getResultList();
	}

	@Override
	public void fechaConexao() {
	}

}
