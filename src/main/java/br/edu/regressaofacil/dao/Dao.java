package br.edu.regressaofacil.dao;

import java.util.List;

public interface Dao<T> {

	void insereObjeto(T objeto);
	
	void atualizaObjeto(T objeto);

	void removeObjeto(int id);

	T getObjeto(int id);

	List<T> getTodosOsObjetos();
	
	void fechaConexao();
}
