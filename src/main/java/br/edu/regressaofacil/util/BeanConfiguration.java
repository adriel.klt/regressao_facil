package br.edu.regressaofacil.util;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.edu.regressaofacil.dao.ModeloDao;
import br.edu.regressaofacil.dao.UsuarioDao;

@Configuration
@ComponentScan
public class BeanConfiguration {

	@Bean
	public ModeloDao getModeloDao() {
		return new ModeloDao();
	}

	@Bean
	public UsuarioDao getUsuarioDao() {
		return new UsuarioDao();
	}

}
