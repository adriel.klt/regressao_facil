package br.edu.regressaofacil.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

public class Interceptador implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		switch (request.getServletPath().substring(1)) {
		case "executa-modelo":
		case "remove-modelo":
		case "modelo":
		case "novo-modelo":
		case "altera-modelo":
		case "cadastraModelo":
		case "alterarModelo":
			Object obj = request.getSession().getAttribute("idUsuario");
			int idUsuario = obj != null ? (Integer) obj : 0;
			if (idUsuario == 0) {
				response.sendError(403);
				return false;
			} else {
				return true;
			}

		default:
			return true;
		}
	}

}
