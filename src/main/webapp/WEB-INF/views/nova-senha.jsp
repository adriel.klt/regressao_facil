<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Regressão Fácil - Entrar</title>
	<link rel="stylesheet" href="<c:url value="assets/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="assets/css/estilo.css"/>" />
</head>
<body>
	<header class="cabecalho ">
		<div class="container row">
			<div class="col">
				<h1><a href="entrar">Regressão fácil</a></h1>
			</div>
			<div class="col">
				<c:if test="${ acessoNegado == true }">
					<h5 class="alert alert-danger texto-centro">Acesso negado!</h5>
				</c:if>
				<form action="autenticaUsuario" method="post">
					<table>
						<tr>
							<td><label>E-mail:</label></td>
							<td><label>Senha:</label></td>
						</tr>
						<tr>
							<td><input class="form-control" type="text" name="email" placeholder="example@gmail.com"/></td>
							<td><input class="form-control" type="password" name="senha" placeholder="MyPassword"/></td>
							<td>
								<button class="btn">Entrar</button>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><a href="esqueci-minha-senha">Esqueci minha senha</a></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</header>
	<section class="section-troca-senha texto-centro">
		<article class="container">
			<h2>Processo para realização da troca da senha do seu usuário</h2>
			<p>Forneça os dados abaixos e em seguida clique em confirmar. Se todos os dados estiverem corretos
			sua senha será alterada e você poderá acessar sua conta! :)</p>
			<c:choose>
				<c:when test="${ trocaSenha == true }">
					<p class="alert alert-success">Senha alterado com sucesso!</p>
				</c:when>
				<c:when test="${ trocaSenha == false }">
					<p class="alert alert-danger">Sua senha não foi alterada, verifique seus dados e tente novamente!</p>
				</c:when>
			</c:choose>
			<div class="estilo-troca-senha">
				<form action="novaSenha" method="post">
					<div class="form-group">
						<label for="email">E-mail:</label>
						<input class="form-control" type="text" name="email" placeholder="example@gmail.com" />
					</div>
					<div class="form-group">
						<Label>Nova senha:</Label>
						<input class="form-control" type="password" name="senha" placeholder="Nova senha"/>
					</div>
					<div class="form-group">
						<Label>Código de verificação:</Label>
						<input class="form-control" type="text" name="codigo" placeholder="Código de verificação"/>
					</div>
					<div class="texto-centro">
						<button class="btn btn-success">Confirmar</button>
					</div>
				</form>
			</div>
		</article>
	</section>
	<footer class="rodape texto-centro">
		<div class="container">
			<p>Todos os direitos reservados @ 2019</p>
		</div>
	</footer>
</body>
</html>