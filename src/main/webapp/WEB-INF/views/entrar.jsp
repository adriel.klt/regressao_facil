<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Regressão Fácil - Entrar</title>
	<link rel="stylesheet" href="<c:url value="assets/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="assets/css/estilo.css"/>" />
</head>
<body>
	<header class="cabecalho">
		<div class="container row">
			<div class="col">
				<h1>
					<a href="entrar">Regressão fácil</a>
				</h1>
			</div>
			<div class="col">
				<c:if test="${ acessoNegado == true }">
					<h5 class="alert alert-danger texto-centro">Acesso negado!</h5>
				</c:if>
				<form action="autenticaUsuario" method="post">
					<table>
						<tr>
							<td>
								<label>E-mail:</label>
							</td>
							<td>
								<label>Senha:</label>
							</td>
						</tr>
						<tr>
							<td>
								<input class="form-control" type="text" name="email" placeholder="example@gmail.com"/>
							</td>
							<td>
								<input class="form-control" type="password" name="senha" placeholder="MyPassword"/>
							</td>
							<td>
								<button class="btn">Entrar</button>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<a href="esqueci-minha-senha">Esqueci minha senha</a>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</header>
	<section class="info-projeto">
		<article class="container">
			<h2>O que é o R?</h2>
			<p>De acordo com o <a target="blank" href="https://www.r-project.org/about.html">r-project</a> o R é um ambiente de software multiplataforma
			e livre que permite realizar computação e redenrização de grafos estatísticos.
			 A ferramenta é baseada na linguagem e ambiente S desenvolvida no laboratórios Bell.
			 Ainda de acordo com o site do projeto é possível pensar no R como sendo uma implementação
			 do S. O R oferece aos seus usuários diversas técnicas estatísticas como 
			 modelagem linear e não linear, testes estatísticos clássicos, 
			 análise de séries temporais, classificação, agrupamento dentre outras.</p>

			<h2>Como eu posso gerar modelos com o regressão fácil?</h2>
			<p>Para a geração de modelos estatísiticos no R é necessário conhecer a linguagem
			R, mas usando o regressão fácil, você dispõe da produção de modelos de regressão
			que fazem uso do <a target="blank" href="https://docs.ufpr.br/~taconeli/CE22517/LivPortugal.pdf">Modelo Linear Generalizado (MLG)</a> de modo gráfico.</p>
			<p>A construção do modelo não retirar do pesquisador a necessidade de entender e
			tratar os seus dados, pois essa aplicação limita-se a produção dos modelos de regressão.
			Para construir um modelo do tipo MLG no R é necessário uma equação que representa a relação
			entre as variáveis, um conjunto de dados e a definição da família de distruibuição e a função de ligação.</p>
		</article>
	</section>
	<section class="section-cadastro">
		<div class="container">
			<h2 class="titulo-cadastro">Cadastre-se e facilite sua vida</h2>
			<c:if test="${ cadastro == true }">
				<h5 class="texto-centro alert alert-success">Cadastrado com sucesso!</h5>
			</c:if>
			<c:if test="${ emailExiste == true }">
				<p class="texto-centro alert alert-warning">Algum usuário já está usando o e-mail fornecido.</p>
			</c:if>
			<p></p>
			<form id="cadastro" action="cadastraUsuario" method="post">
				<fieldset class="container">
					<div class="form-group">
						<label for="nome">Nome: <f:errors path="usuario.nome" cssClass="erro-cadastro"/></label>
						<input class="form-control" type="text" id="nome" name="nome" placeholder="Nome completo" />
					</div>
					<div class="form-group">
						<label for="email">E-mail: <f:errors path="usuario.email" cssClass="erro-cadastro"/></label>
						<input class="form-control" type="text" id="email" name="email" placeholder="E-mail" />
					</div>
					<div class="form-group">
						<label for="senha">Senha: <f:errors path="usuario.senha" cssClass="erro-cadastro"/></label>
						<input class="form-control" type="password" id="senha" name="senha" placeholder="Senha" />
					</div>
					<div style="text-align:center">
						<button class="btn btn-primary">Cadastrar</button>
					</div>
				</fieldset>
			</form>
		</div>
	</section>
	<footer class="rodape texto-centro">
		<div class="container">
			<p style="text-align:center">Todos os direitos reservados @ 2019</p>
		</div>
	</footer>
<script>
	if (window.location.href.endsWith('cadastraUsuario')) {
		window.scroll({       // 1
		    top: document
		  .querySelector( '#cadastro' )
		    .offsetTop,       // 2
		    left: 0,
		    behavior: 'smooth'// 3
		 });
	}
</script>
</body>
</html>