<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Regressão Fácil - Entrar</title>
	<link rel="stylesheet" href="<c:url value="assets/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="assets/css/estilo.css"/>" />
</head>
<body>
	<header class="cabecalho ">
		<div class="container row">
			<div class="col">
				<h1><a href="entrar">Regressão fácil</a></h1>
			</div>
			<div class="col">
				<c:if test="${ acessoNegado == true }">
					<h5 class="alert alert-danger texto-centro">Acesso negado!</h5>
				</c:if>
				<form action="autenticaUsuario" method="post">
					<table>
						<tr>
							<td><label>E-mail:</label></td>
							<td><label>Senha:</label></td>
						</tr>
						<tr>
							<td><input class="form-control" type="text" name="email" placeholder="example@gmail.com"/></td>
							<td><input class="form-control" type="password" name="senha" placeholder="MyPassword"/></td>
							<td>
								<button class="btn">Entrar</button>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><a href="esqueci-minha-senha">Esqueci minha senha</a></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</header>
	<section class="section-troca-senha texto-centro">
		<article class="container">
			<h2>Processo para realização da troca da senha do seu usuário</h2>
			<p>Forneça abaixo o endereço de E-mail utilizado por você para realizar seu
			cadastro na plaforma. Você receberá um E-mail com um código de verificação e
			um link para que você possa realiza a troca da sua senha.</p>
			<c:choose>
				<c:when test="${ solicitacao == true }">
					<p class="alert alert-success">Solicitação efetuada com sucesso!</p>
				</c:when>
				<c:when test="${ solicitacao == false }">
					<p class="alert alert-danger">O E-mail fornecedio não pertence a nenhum
					usuário cadastrado!</p>
				</c:when>
			</c:choose>
			<form action="solicitaTrocaSenha" method="post">
				<input class="form-control ajuste-input-text-email" type="text" name="email" placeholder="example@gmail.com" />
				<button class="btn btn-primary">Enviar</button>
			</form>
		</article>
	</section>
	<footer class="rodape texto-centro">
		<div class="container">
			<p>Todos os direitos reservados @ 2019</p>
		</div>
	</footer>
</body>
</html>