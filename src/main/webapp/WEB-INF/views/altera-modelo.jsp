<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Regressão Fácil - Altera Modelo</title>
	<link rel="stylesheet" href="<c:url value="assets/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="assets/css/estilo.css"/>" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<nav class="navbar navbar-expand-sm">
		<h1 class="navbar-brand logo"><a href="modelo" style="text-decoration: none; color: white;">Regressão fácil</a></h1>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active"><a class="nav-link" href="#" style="text-decoration: none; color:white;"><strong>Seja bem-vindo, ${ nomeUsuario }</strong></a></li>
			<li class="nav-item active"><a class="nav-link link-menu"  href="novo-modelo">Novo Modelo</a></li>
			<li class="nav-item active"><a class="nav-link link-menu"  href="logout">Sair</a></li>
		</ul>
	</nav>
	<section class="container">
		<c:if test="${ alteraModelo == false}">
			<h5 class="alert alert-danger texto-centro">Preencha todos os campos e forneça um arquivo do tipo 'CSV'!</h5>
		</c:if>
		<c:if test="${ alteraModelo == true}">
			<h5 class="alert alert-success texto-centro">Alterado com sucesso!</h5>
		</c:if>
		<div style="padding: 5%;">
			<h2 class="titulo-cadastro-modelo">Aleteração de um modelo</h2>
			<form action="alterarModelo" method="post" enctype="multipart/form-data">
				<input type="hidden" value="${ modelo.id }"  name="id"/>
				<div class="form-group">
					<label>Nome: <f:errors path="modelo.nome" cssClass="erro-cadastro-modelo"/></label>
					<input type="text" name="nome" class="form-control" value="${ modelo.nome }"/>
				</div>
				<div class="form-group">
					<label>Modelo: <f:errors path="modelo.formula" cssClass="erro-cadastro-modelo"/></label>
					<input type="text" name="formula" class="form-control" value="${ modelo.formula }"/>
				</div>
				<div class="form-group">
					<label>Função de ligação:</label>
					<select name="funcaoLigacao" class="form-control">
						<c:choose>
							<c:when test="${ modelo.funcaoLigacao == 'binomial' }">
								<option selected value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'gaussian' }">
								<option value="binomial">Binomial</option>
								<option selected value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'Gamma' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option selected value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'invese.gaussian' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option selected value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'poisson' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option selected value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'quasi' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option selected value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'quasibinomial' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option selected value="quasibinomial">Quasi Binomial</option>
								<option value="quasipoisson">Quasi Poisson</option>
							</c:when>
							<c:when test="${ modelo.funcaoLigacao == 'quasipoisson' }">
								<option value="binomial">Binomial</option>
								<option value="gaussian">Gaussian</option>
								<option value="Gamma">Gamma</option>
								<option value="invese.gaussian">Inverse Gaussian</option>
								<option value="poisson">Poisson</option>
								<option value="quasi">Quasi</option>
								<option value="quasibinomial">Quasi Binomial</option>
								<option selected value="quasipoisson">Quasi Poisson</option>
							</c:when>
						</c:choose>
					</select>
				</div>
				<div class="form-group">
					<label>Família de distribuição:</label>
					<select name="familiaDistribuicao" class="form-control">
						<c:choose>
							<c:when test="${ modelo.familiaDistribuicao == 'logit'}">
								<option selected value="logit">Logit</option>
								<option value="identity">Identity</option>
								<option value="inverse">Inverse</option>
								<option value="log">Log</option>
							</c:when>
							<c:when test="${ modelo.familiaDistribuicao == 'identity'}">
								<option value="logit">Logit</option>
								<option selected value="identity">Identity</option>
								<option value="inverse">Inverse</option>
								<option value="log">Log</option>
							</c:when>
							<c:when test="${ modelo.familiaDistribuicao == 'inverse'}">
								<option value="logit">Logit</option>
								<option value="identity">Identity</option>
								<option selected value="inverse">Inverse</option>
								<option value="log">Log</option>
							</c:when>
							<c:when test="${ modelo.familiaDistribuicao == 'log'}">
								<option value="logit">Logit</option>
								<option value="identity">Identity</option>
								<option value="inverse">Inverse</option>
								<option selected value="log">Log</option>
							</c:when>
						</c:choose>
						
					</select>
				</div>
				<div class="form-group">
					<label>Arquivo (CSV):</label>
					<input type="file" name="arquivo" accept=".csv, text/csv" class="form-control"/>
				</div>
				<div class="texto-centro">
					<button class="btn btn-primary">Alterar</button>
				</div>
			</form>
		</div>
	</section>
	<footer class="rodape texto-centro">
		<div class="container">
			<p>Todos os direitos reservados @ 2019 </p>
		</div>
	</footer>
</body>
</html>