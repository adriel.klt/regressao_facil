<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width" />
	<title>Regressão Fácil - Modelos</title>
	<link rel="stylesheet" href="<c:url value="assets/css/bootstrap.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="assets/css/estilo.css"/>" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<body>
	<nav class="navbar navbar-expand-sm">
		<h1 class="navbar-brand logo">
			<a href="modelo" style="text-decoration: none; color: white;">Regressão fácil</a>
		</h1>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a class="nav-link" href="#" style="text-decoration: none; color: white; ">
					<strong>Seja bem-vindo, ${ nomeUsuario }</strong>
				</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link link-menu"  href="novo-modelo">Novo Modelo</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link link-menu"  href="logout">Sair</a>
			</li>
		</ul>
	</nav>
	<section class="container" style="padding: 5% 10px;" id="main-section">
		<c:if test="${ fn:length(usuario.modelos) == 0 }">
			<div style="padding: 10%">
				<h2 class="alert alert-warning texto-centro">Você não possui nenhum modelo!</h2>
			</div>
		</c:if>
		<div class="row" id="row">
		<c:forEach var="modelo" items="${ usuario.modelos }">
			<div class="col-md-4" id="${ modelo.id }">
				<div class="card bg-light mb-3" style="">
				  	<div class="card-header">${ modelo.nome }</div>
				  	<div class="card-body">
				   		<p class="card-text">
				   			<strong>Modelo:</strong> ${ modelo.formula }
						</p>
						<p>
							<strong>Família de distribuição:</strong> ${ modelo.familiaDistribuicao }
						</p>
						<p>
							<strong>Função de ligação:</strong> ${ modelo.funcaoLigacao }
						</p>
						<div class="texto-centro">
							<a class="btn btn-success" onClick="geraModelo(${modelo.id})" title="Gerar relatório do modelo">
								<i class="material-icons" style="color: black;">assignment</i>
							</a>
							<a class="btn btn-warning" href="altera-modelo?idModelo=${ modelo.id }" title="Editar modelo">
								<i class="material-icons">mode_edit</i>
							</a>
							<a class="btn btn-danger"  onClick="removeModelo(${ modelo.id});"  title="Remover modelo">
								<i class="material-icons">delete</i>
							</a>
						</div>
						<hr/>
							<h5 style="text-align:center">Área informativa</h5>
						<hr/>
						<div class="texto-centro load load${modelo.id}">
							<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>
							Carregando...
						</div>
						<div class="load texto-centro erro${modelo.id}">
							<p>Houve um erro na geração do modelo</p>
							<ol>
								<li>Verifique se você está usando os parâmetros da fórmula corretamente.</li>
								<li>Verifique se os valores atendem as especificidades da família de distruibuição.</li>
								<li>Verifique se os valores atendem as especificidades da função de ligação.</li>
							</ol>
						</div>
				  	</div>
				</div>
			</div>
		</c:forEach>
		</div>
	</section>
	<footer class="rodape texto-centro">
		<div class="container">
			<p>Todos os direitos reservados @ 2019 </p>
		</div>
	</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script>
	
		$(document).ready(function() {
			$('.load').hide();
		});
	
		function removeModelo(idModelo) {
			if (window.confirm('Você realmente deseja excluir este modelo?') == true) {
				var request = new XMLHttpRequest();
				request.open("GET", "remove-modelo?idModelo="+idModelo, true);
				request.send();
				request.onreadystatechange = function() {
					if (request.readyState == 4 && request.status == 200) {
						$('#'+idModelo).remove();
					}
					parent = document.getElementById('row');
					if (!parent.hasChildNodes()) {
						$('#main-section').append("<div style='padding: 10%'><h2 class='alert alert-warning texto-centro'>Você não possui nenhum modelo!</h2></div>");
					}
				}
			}
		}
		
		function geraModelo(idModelo) {
			var request = new XMLHttpRequest();
			request.open("GET", "executa-modelo?idModelo="+idModelo, true);
			request.responseType = "blob";
			request.send();
			$(('.load'+idModelo)).show();
			$(('.erro'+idModelo)).hide();
		
			request.onreadystatechange = function() {
				if (request.readyState == 4 && request.status == 200) {
					$(".load"+idModelo).hide();
					$(('.erro'+idModelo)).hide();
					var link = document.createElement('a');
					var blob = request.response;
				      link.href = window.URL.createObjectURL(blob);
				      link.download = "Preview.pdf";
				      link.click();
				} else if (request.status == 500) {
					$(('.load'+idModelo)).hide();
					$(('.erro'+idModelo)).show();
				}
			}
		}
		
	</script>
</body>
</html>